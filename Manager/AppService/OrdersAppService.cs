﻿using Core.Models.Order;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Persistence.Reader;
using Persistence.Writer;
using RXWebAPIWS.LogClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FidelizeWebAPIWS.Manager.AppService
{    
    public class OrdersAppService
    {
        private readonly IConfiguration _config;
        private readonly string _connectionString;
        private readonly string _urlRxIntegration;
        private readonly int _idIntegracao;
        private static Log logPedido = new Log("CallPedidoLog", "CallPedidoLog");

        public OrdersAppService(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("Default");
            _urlRxIntegration = _config.GetValue<string>("RxIntegration");
            _idIntegracao = _config.GetValue<int>("IdIntegracao");
        }

        public async Task<bool> ProcessaPedido(Order order)
        {
            HttpClient client = new HttpClient();
            var url = _urlRxIntegration;
            order.id_integracao = _idIntegracao;

            var xablau = JsonConvert.SerializeObject(order, Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

            try
            {
                var JsonPutRxAPI = new StringContent(xablau, Encoding.UTF8, "application/json");

                using (var responseAPI = client.PutAsync(url, JsonPutRxAPI).Result)
                {
                    string log = await responseAPI.Content.ReadAsStringAsync();
                    logPedido.WriteEntry(log);
                    responseAPI.EnsureSuccessStatusCode();
                    Console.WriteLine("Pedido Inserido");
                }

                    return true;
            }
            catch (Exception ex)
            {
                logPedido.WriteEntry(ex);
                throw new Exception(ex.Message);
            }
        }

        public async Task<IActionResult> ProcessaPedidoRetornoResponse(Order order, string situacao, string filial)
        {
            try
            {
                Order storedEntity;
                using (var OrderReader = new OrderReaderRepository(_connectionString))
                using (var OrderWriter = new OrderWriterRepository(_connectionString))
                {
                    storedEntity = await OrderReader.OrderFiltersQuery(order.cod_pedido, situacao, filial);
                    if (storedEntity == null)
                    {
                        throw new Exception($"O pedido {order.cod_pedido} não existe.");
                    }

                    await OrderWriter.OrderLogUpdateResponse(order.cod_pedido);
                }

                return new OkResult();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IActionResult> ProcessaPedidoRetornoCancellation(Order order)
        {
            try
            {                
                Order storedLogIntegracao;

                using (var OrderReader = new OrderReaderRepository(_connectionString))
                using (var OrderWriter = new OrderWriterRepository(_connectionString))
                {
                    storedLogIntegracao = await OrderReader.OrderLogIdIntegracaoQuery(order.cod_pedido_secundario);
                    if (storedLogIntegracao.id_log == null || storedLogIntegracao.id_log == 0)
                    {
                        throw new Exception($"The Order {order.cod_pedido} doesn't exist !");
                    }
                    await OrderWriter.OrderLogStatusUpdateCancellation(storedLogIntegracao.id_log);
                }
                return new OkResult();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
