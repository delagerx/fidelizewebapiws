﻿using Core.Models;
using Core.Models.Invoices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Persistence.Reader;
using Persistence.Writer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RXWebAPIWS.Manager.AppService
{
    public class InvoicesAppService
    {
        private readonly IConfiguration _config;
        private readonly string _connectionString;

        public InvoicesAppService(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("Default");
        }

        public async Task<IActionResult> ProcessaTitulo(Invoice invoice)
        {
            try
            {
                int? client_code = invoice.OrderId;
                int? cod_pedido;
                int? idInvoice;
                int? subsidiaryId;
                Invoice storedEntity;
                int? logId;

                using (InvoiceReaderRepository InvoiceReader = new InvoiceReaderRepository(_connectionString))
                {
                    storedEntity = await InvoiceReader.InvoiceFiltersQuery(client_code);
                    if(storedEntity == null)
                    {
                        throw new Exception($"O título relacionado ao pedido {invoice.Client_Code} não existe.");
                    }
                    cod_pedido = storedEntity.Client_Code;
                    idInvoice = storedEntity.InvoiceId;
                    subsidiaryId = storedEntity.SubsidiaryId;                    
                    logId = await InvoiceReader.InvoiceLogIdQuery(idInvoice, subsidiaryId);
                }

                using (InvoiceWriterRepository InvoiceWriter = new InvoiceWriterRepository(_connectionString))
                {
                    if (logId == 0)
                        logId = await InvoiceWriter.InvoiceLogInsert(idInvoice, subsidiaryId, cod_pedido);
                    else
                        await InvoiceWriter.InvoiceLogUpdate(idInvoice, subsidiaryId, cod_pedido);

                    if (invoice.Errors == null) return new OkResult();
                    foreach(Error error in invoice.Errors)
                    {
                        await InvoiceWriter.InvoiceLogErrorInsert(logId, error.ErrorCode, error.ErrorMessenger);
                    }
                }

                return new OkResult();
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains(invoice.Client_Code.ToString()))
                    throw new Exception(ex.Message);
                throw new Exception($"Um erro ocorreu no título {invoice.Client_Code}");
            }
        }
    }
}
