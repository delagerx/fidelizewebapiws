﻿using Core.Models.Invoices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Persistence.Reader;
using RXWebAPIWS.Manager.AppService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RXWebAPIWS.Controllers
{
    [Produces("application/json")]
    [Route("/api/WS/InvoiceWS")]
    public class InvoicesController : Controller
    {
        private readonly IConfiguration _config;
        private readonly string _connectionString;

        public InvoicesController(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("Default");
        }

        [HttpGet]
        public async Task<IEnumerable<Invoice>> GetInvoice()
        {
            try
            {
                using (InvoiceReaderRepository InvoiceReader = new InvoiceReaderRepository(_connectionString))
                {
                    IEnumerable<Invoice> invoices = await InvoiceReader.GetInvoices();

                    foreach (Invoice invoice in invoices)
                    {
                        invoice.Products = await InvoiceReader.InvoiceProductsQuery(invoice.InvoiceId);
                    }

                    return invoices;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] IEnumerable<Invoice> Invoice)
        {
            InvoicesAppService InvoiceAppService = new InvoicesAppService(_config);
            List<BadRequestObjectResult> listSkuErrors = new List<BadRequestObjectResult>();

            foreach (var order in Invoice)
            {
                try
                {
                    await InvoiceAppService.ProcessaTitulo(order);
                }
                catch (Exception ex)
                {
                    listSkuErrors.Add(new BadRequestObjectResult(ex.Message));
                }
            }

            if (listSkuErrors.Count > 0)
            {
                return BadRequest(listSkuErrors);
            }

            return Ok();
        }


    }
}
