﻿using Core.Models.Invoices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Persistence.Reader;
using RXWebAPIWS.Manager.AppService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RXWebAPIWS.Controllers
{
    [Produces("application/json")]
    [Route("/api/WS/InvoiceReversalWS")]
    public class InvoiceReversalController : Controller
    {
        private readonly IConfiguration _config;
        private readonly string _connectionString;
        private readonly int _subsidiaryId;

        public InvoiceReversalController(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("Default");
        }

        [HttpGet]
        public async Task<IEnumerable<Invoice>> GetInvoiceReversal()
        {
            using (InvoiceReaderRepository InvoiceReader = new InvoiceReaderRepository(_connectionString))
            {
                try
                {
                    IEnumerable<Invoice> invoices = await InvoiceReader.InvoiceReversalQuery();

                    foreach(Invoice invoice in invoices)
                    {
                        invoice.Products = await InvoiceReader.InvoiceProductsQuery(invoice.InvoiceId);
                    }
                    return invoices;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] IEnumerable<Invoice> Invoices)
        {
            InvoicesAppService InvoiceAppService = new InvoicesAppService(_config);
            List<BadRequestObjectResult> listSkuErrors = new List<BadRequestObjectResult>();

            foreach(Invoice invoice in Invoices)
            {
                try
                {
                    await InvoiceAppService.ProcessaTitulo(invoice);
                }
                catch(Exception ex)
                {
                    listSkuErrors.Add(new BadRequestObjectResult(ex.Message));
                }
            }

            if(listSkuErrors.Count > 0)
            {
                return BadRequest(listSkuErrors);
            }

            return Ok();
        }
    }
}
