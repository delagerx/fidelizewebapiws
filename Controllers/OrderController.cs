﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models.Order;
using FidelizeWebAPIWS.Manager.AppService;
using Persistence.Reader;

namespace FidelizeWebAPIWS.Controllers
{
    [Produces("application/json")]
    [Route("/api/WS")]
    public class OrderController : Controller
    {
        private readonly IConfiguration _config;
        private readonly string _connectionString;

        public OrderController(IConfiguration configuration)
        {
            _config = configuration;
            _connectionString = _config.GetConnectionString("Default");
        }

        //GET: api/WS
        //[HttpPut()]
        [HttpPut("orderPut")]
        public async Task<IActionResult> Put([FromBody] IEnumerable<Order> Order)
        {
            OrdersAppService OrderAppService = new OrdersAppService(_config);
            List<BadRequestObjectResult> listSkuErrors = new List<BadRequestObjectResult>();

            foreach (Order order in Order)
            {
                try
                {
                    await OrderAppService.ProcessaPedido(order);
                }
                catch (Exception ex)
                {
                    listSkuErrors.Add(new BadRequestObjectResult(ex.Message));
                }
            }

            if (listSkuErrors.Count > 0)
            {
                return BadRequest(listSkuErrors);
            }

            return Ok();

        }

        [HttpGet("createResponse")]
        public async Task<IEnumerable<Order>> GetResponseAsync(string subsidiaryIds, string situation)
        {
            IEnumerable<Order> orders;

            using (OrderReaderRepository OrderReader = new OrderReaderRepository(_connectionString))
            {
                orders = await OrderReader.OrderResponse(subsidiaryIds, situation);

                foreach (var order in orders)
                {
                    order.Products = await OrderReader.OrderResponseProducts(Convert.ToInt32(order.cod_pedido));
                }
            }

            return orders;
        }

        [HttpPut("OrderResponse")]
        public async Task<IActionResult> OrderResponseAsync([FromBody] IEnumerable<Order> Order, string situacaoRetorno, string filial)
        {
            OrdersAppService OrdersAppService = new OrdersAppService(_config);
            List<BadRequestObjectResult> listSkuErrors = new List<BadRequestObjectResult>();

            foreach (var order in Order)
            {
                try
                {
                    await OrdersAppService.ProcessaPedidoRetornoResponse(order, situacaoRetorno, filial);
                }
                catch (Exception ex)
                {
                    listSkuErrors.Add(new BadRequestObjectResult(ex.Message));
                }
            }

            if (listSkuErrors.Count > 0)
            {
                return BadRequest(listSkuErrors);
            }

            return new OkResult();
        }

        [HttpGet("createCancellation")]
        public async Task<IEnumerable<Order>> GetOrderCancellation(string subsidiaryIds, string situationCancellation)
        {
            using (OrderReaderRepository OrderReader = new OrderReaderRepository(_connectionString))
            {
                IEnumerable<Order> orders = await OrderReader.OrderCancellationQuery(subsidiaryIds, situationCancellation);

                foreach (Order order in orders)
                {
                    order.Products = await OrderReader.OrderItemsCancellationQuery(order.cod_pedido);
                }
                return orders;
            }
        }

        [HttpPut("OrderCancellation")]
        public async Task<IActionResult> OrderCancellationAsync([FromBody] IEnumerable<Order> Order)
        {
            OrdersAppService OrderAppService = new OrdersAppService(_config);
            List<BadRequestObjectResult> listSkuErrors = new List<BadRequestObjectResult>();

            foreach (Order order in Order)
            {
                try
                {
                    await OrderAppService.ProcessaPedidoRetornoCancellation(order);
                }
                catch (Exception ex)
                {
                    listSkuErrors.Add(new BadRequestObjectResult(ex.Message));
                }
            }

            if (listSkuErrors.Count > 0)
            {
                return BadRequest(listSkuErrors);
            }

            return Ok();
        }
    }
}
