﻿using System.Linq;
using System.Threading.Tasks;
using Persistence.DapperContext;
using System.Data.SqlClient;
using Dapper;

namespace Persistence.Writer
{
    public class OrderWriterRepository : DapperEscritorContext
    {
        public OrderWriterRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task OrderLogUpdateResponse(int? id_log)
        {
            var sql = @"UPDATE log_pedido_rx_api
                        SET data_response = GETDATE()
                        WHERE id_log = @id_log";

            var param = new
            {
                id_log
            };

            await Connection.QueryAsync<int>(sql, param, Transaction);
            Commit();
        }

        public async Task OrderLogStatusUpdateCancellation(int? id)
        {
            string sql = @"UPDATE log_pedido_rx_api
	                          SET data_cancellation = GETDATE()
	                        WHERE id_log = @Id";

            var param = new
            {
                @Id = id,
            };

            await Connection.QueryAsync<int>(sql, param, Transaction);
            Commit();
        }
    }
}
