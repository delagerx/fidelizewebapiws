﻿using System.Linq;
using Dapper;
using System.Threading.Tasks;
using Persistence.DapperContext;


namespace Persistence.Writer
{
    public class InvoiceWriterRepository : DapperEscritorContext
    {
        public InvoiceWriterRepository(string connectionString) : base (connectionString)
        {
        }

        public async Task<int> InvoiceLogInsert(int? invoiceId, int? subsidiaryId, int? codPedido)
        {
            string sql = @"INSERT log_integracao_titulo (cod_titulo, data_exportacao,cod_pedido, cod_filial, id_integracao)
	                    VALUES(@InvoiceId, GETDATE(),@CodPedido, @SubsidiaryId, 2)
                    

                        UPDATE log_pedido_rx_api
                           SET data_invoice = GETDATE()
                         WHERE cod_pedido = @CodPedido


                        SELECT SCOPE_IDENTITY()";

            var param = new
            {
                InvoiceId = invoiceId,
                SubsidiaryId = subsidiaryId,
                CodPedido = codPedido
            };

            int id = await Connection.QueryFirstOrDefaultAsync<int>(sql, param, Transaction);
            Commit();
            return id;
        }

        public async Task InvoiceLogUpdate(int? invoiceId, int? subsidiaryId, int? clientCode)
        {
            string sql = @"UPDATE log_integracao_titulo
	                          SET data_exportacao = GETDATE()
	                        WHERE cod_titulo = @InvoiceId
                              AND cod_filial = @SubsidiaryId


                            UPDATE log_pedido_rx_api
                               SET data_invoice = GETDATE()
                             WHERE cod_filial_faturamento_rel = @subsidiaryId
                               AND cod_pedido = @ClientCode";

            var param = new
            {
                InvoiceId = invoiceId,
                SubsidiaryId = subsidiaryId,
                ClientCode = clientCode
            };

            await Connection.QueryAsync<int>(sql, param, Transaction);
            Commit();
        }

        public async Task InvoiceLogErrorInsert(int? logId, int errorCode, string errorMessenger)
        {
            var sql = @"INSERT INTO log_integracao_erro_titulo(cod_pendencia, pendencia, codd_log)
                             VALUES (@ErrorCode, @ErrorMessenger, @LogId)";

            await Connection.ExecuteAsync(sql, new
            {
                LogId = logId,
                ErrorCode = errorCode,
                ErrorMessenger = errorMessenger
            }, Transaction);

            Commit();
        }
    }
}
