﻿using Persistence.DapperContext;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Core.Models.Invoices;
using System.Threading.Tasks;

namespace Persistence.Reader
{
    public class InvoiceReaderRepository : DapperLeitorContext
    {
        public InvoiceReaderRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<Invoice>> GetInvoices()
        {
            try
            {
                string sql = @"SELECT	 lpa.cod_industria AS 'IndustryCode'
									,t.cod_titulo as 'InvoiceId'
                                    ,t.numero_titulo as 'Invoice_number'
			                        ,t.data_cadastro as 'Emission'
									,t.data_cadastro AS 'ProcessedAt'
									,lpa.cod_filial_faturamento_rel AS 'Subsidiary_CNPJ'
									,lpa.cod_cliente_rel AS 'Client_CNPJ'
                                    ,lpa.cod_pedido_primario AS 'Wholesaler_Order_Code'
			                        ,lpa.cod_pedido_secundario AS 'OrderId'
			                        ,t.numero_titulo AS 'Invoice_number'                                    
                                    ,isnull(ttb_base_totalnf.base,0.00) AS 'Invoice_Value'
                                    ,isnull(t.desconto, 0) AS 'Invoice_discount'
			                        ,t.volumes AS 'Invoice_volume_amount' --​Quantidade de Volume products
                                    ,t.chave_acesso AS 'invoice_danfe_key'		
                            FROM	titulo t(NOLOCK)
	                        INNER JOIN entidade f(NOLOCK) ON t.cod_filial_estoque = f.cod_entidade
							INNER JOIN 	log_pedido_rx_api lpa on  	lpa.cod_pedido = t.cod_pedido													                        
                            INNER JOIN titulo_tributacao_bases ttb_base_totalnf (NOLOCK)  on ttb_base_totalnf.cod_titulo = t.cod_titulo
                            																		AND ttb_base_totalnf.tipo_base = 9	
							LEFT JOIN	log_integracao_titulo lit (NOLOCK) ON lit.cod_titulo = T.cod_titulo	                        
	                        WHERE	 t.cod_situacao = 4 
	                        AND operacao_pedido = 0 
	                        AND	(t.cod_titulo NOT IN (SELECT cod_titulo FROM log_integracao_titulo WHERE cod_titulo = t.cod_titulo))
                            AND lpa.data_invoice IS NULL";

                return await Connection.QueryAsync<Invoice>(sql);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<InvoiceProduct>> InvoiceProductsQuery(int? invoiceId)
        {
            string sql = @"SELECT 	  cx.cod_barra as EAN						
	                                , ti.quantidade as Quantity
	                                , ti.pdu as Percent_discount_product
		                            , (ti.preco-ti.valor_unitario) as DiscountValueProduct 
	                                , ti.valor_unitario as UnitPriceProduct	                                      
                            FROM	titulo t(NOLOCK)
                            INNER JOIN titulo_item ti ON t.cod_titulo = ti.cod_titulo
                            INNER JOIN produto p(NOLOCK) ON P.cod_produto = ti.cod_produto
                            INNER JOIN caixa_fechada cx (NOLOCK) ON cx.cod_produto = p.cod_produto
                                                                AND cx.principal = -1                           
                    WHERE t.cod_titulo = @InvoiceId";

            var param = new
            {
                InvoiceId = invoiceId
            };
            return await Connection.QueryAsync<InvoiceProduct>(sql, param);

        }

        public async Task<Invoice> InvoiceFiltersQuery(int? client_code)
        {
            string sql = @"SELECT								lpr.cod_pedido AS 'Client_Code',
									 t.cod_titulo as 'InvoiceId'
			                        ,t.data_cadastro as 'emission'
			                        ,t.numero_titulo as 'invoice_number'
                                    ,t.chave_acesso as 'invoice_danfe_key'			                
			                        ,t.volumes as 'invoice_volume_amount' --​Quantidade de Volume products
                                    ,t.cod_filial_estoque as 'subsidiaryId'
                        FROM	titulo t(NOLOCK)
	                        INNER JOIN entidade f(NOLOCK) ON t.cod_filial_estoque = f.cod_entidade
							INNER JOIN 	log_pedido_rx_api lpr on  	lpr.cod_pedido = t.cod_pedido	
												                        
							LEFT JOIN	log_integracao_titulo lit (NOLOCK) ON lit.cod_titulo = T.cod_titulo
																																																																																																														
	                        										
                        WHERE	 t.cod_situacao = 4 
	                        AND operacao_pedido = 0 
	                        AND	lit.data_exportacao IS NULL									
                            AND lpr.cod_pedido_secundario = @OrderId";

            var param = new
            {
                OrderId = client_code
            };

            return await Connection.QueryFirstOrDefaultAsync<Invoice>(sql, param);
        }

        public async Task<int> InvoiceLogIdQuery(int? invoiceId, int? subsidiaryId)
        {
            var sql = @"SELECT isnull(id_log_titulo,0) as LogId
                          FROM log_integracao_titulo
                         WHERE cod_titulo = @InvoiceId
                           AND cod_filial = @SubsidiaryId";

            var param = new
            {
                InvoiceId = invoiceId,
                SubsidiaryId = subsidiaryId
            };

            return await Connection.QuerySingleOrDefaultAsync<int>(sql, param);
        }

        public async Task<IEnumerable<Invoice>> InvoiceReversalQuery()
        {
            string sql = @"DECLARE @tempTable TABLE(cod_invoice_reversal int, cod_pedido int)
		            INSERT INTO @tempTable (cod_invoice_reversal,cod_pedido)
		            SELECT isnull(t.numero_titulo,0) as cod_invoice_reversal ,lit.cod_pedido as cod_pedido  
		            from log_integracao_titulo lit inner join titulo t on t.cod_titulo = lit.cod_titulo AND t.cod_situacao = 6 order by lit.data_exportacao DESC 
		                    SELECT TOP 1 tb.cod_invoice_reversal
									,lpr.cod_industria AS industryCode
									,lpr.cod_pedido_secundario AS orderId
									,t.cod_pedido 
									,t.cod_titulo AS invoiceId
			                        ,t.data_cadastro AS processedAt
									,t.data_cadastro AS emission
			                        ,lpr.cod_pedido_primario AS wholesaler_Order_Code
			                        ,t.numero_titulo AS invoice_number
                                    ,t.chave_acesso AS invoice_danfe_key 
			                        ,isnull(ttb_base_totalnf.base,0.00) as invoice_Value
			                        ,t.volumes AS invoice_volume_amount
									,lpr.cod_cliente_rel AS client_CNPJ
									,lpr.cod_filial_faturamento_rel AS subsidiary_CNPJ
                                    ,isnull(t.desconto, 0) AS 'Invoice_discount'
                        FROM	titulo t(NOLOCK)
	                        INNER JOIN entidade f(NOLOCK) ON t.cod_filial_estoque = f.cod_entidade
							INNER JOIN 	log_pedido_rx_api lpr on  	lpr.cod_pedido = t.cod_pedido	
																		AND lpr.data_response IS NOT NULL 
																		AND lpr.data_invoice IS NOT null
							INNER JOIN @tempTable tb on lpr.cod_pedido = tb.cod_pedido
	                        INNER JOIN entidade_doc ed_filial(NOLOCK) ON ed_filial.cod_entidade = t.cod_filial_estoque
												                        AND ed_filial.cod_tipo_doc = 3
	                        INNER JOIN entidade_doc ed_cliente(NOLOCK) ON ed_cliente.cod_entidade = t.cod_cliente
																		AND ed_cliente.cod_tipo_doc = 3
							INNER JOIN titulo_tributacao_bases ttb_base_totalnf (NOLOCK)  on ttb_base_totalnf.cod_titulo = t.cod_titulo
																		AND ttb_base_totalnf.tipo_base = 9									
                        WHERE 
                            t.cod_situacao = 4
	                        AND t.operacao_pedido = 0
							AND t.cod_titulo not in(select cod_titulo from log_integracao_titulo)
                            ORDER BY emission DESC";

            return await Connection.QueryAsync<Invoice>(sql);
        }
    }
}
