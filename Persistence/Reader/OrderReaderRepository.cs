﻿using Persistence.DapperContext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Models.Order;
using Dapper;


namespace Persistence.Reader
{
    public class OrderReaderRepository : DapperLeitorContext
    {
        public OrderReaderRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<Order>> OrderResponse(string subsidiaryIds, string situation)
        {
            //   string sql = @"SELECT lpa.cod_pedido,
            //                         lpa.cod_industria, 
            //           	          lpa.cod_pedido_secundario, 
            //           	          lpa.cod_filial_faturamento_rel,
            //           	          lpa.data_cadastro
            //                  FROM pedido p
            //INNER JOIN log_pedido_rx_api lpa ON p.cod_pedido = lpa.cod_pedido
            //                  LEFT JOIN titulo t ON p.cod_pedido = t.cod_pedido 
            //                  WHERE NOT EXISTS(SELECT 1
            //                                   FROM log_integracao_titulo lit (NOLOCK) 
            //                                   WHERE lit.cod_titulo = T.cod_titulo
            //                                   AND lit.data_exportacao IS NOT NULL)
            //					AND lpa.data_response IS NULL ";

            string sql = @"SELECT TOP 10 lpa.cod_pedido,
	                               lpa.cod_industria, 
	                               lpa.cod_pedido_secundario, 
	                               lpa.cod_filial_faturamento_rel,
	                               lpa.data_cadastro,
                                   lpa.id_log
                            FROM log_pedido_rx_api lpa
                            LEFT JOIN pedido p (NOLOCK) ON p.cod_pedido = lpa.cod_pedido
                            LEFT JOIN titulo t (NOLOCK) ON t.cod_pedido = lpa.cod_pedido
                             WHERE NOT EXISTS(SELECT 1
				                                FROM log_integracao_titulo lit (NOLOCK) 
				                               WHERE lit.cod_titulo = T.cod_titulo
				                                 AND lit.data_exportacao IS NOT NULL)
				                                 AND lpa.data_response IS NULL
                                                 AND ISNULL(p.digitando, 0) <> 1";

            return await Connection.QueryAsync<Order>(sql);
        }

        public async Task<IEnumerable<OrderProduct>> OrderResponseProducts(int id_log)
        {
            //string sql = @"SELECT  cx.cod_barra
            //          , pi.separado as quantidade
            //          , pi.pdu as pdu
            //          , (pi.preco - pi.valor_mercadoria) as valor_desconto
            //          , pi.valor_mercadoria
            //          , 0 as monitorado
            //                , CONVERT(VARCHAR, pi.motivo) as motivo
            //        FROM pedido ped(NOLOCK)
            //            INNER JOIN pedido_item pi ON ped.cod_pedido = pi.cod_pedido
            //            INNER JOIN produto p(NOLOCK) ON P.cod_produto = PI.cod_produto
            //         INNER JOIN caixa_fechada cx (NOLOCK) ON cx.cod_produto = p.cod_produto											                    
            //            INNER JOIN log_pedido_item_rx_api log (NOLOCK) ON cx.cod_barra = log.cod_produto_rel
            //        WHERE ped.cod_pedido = @cod_pedido";

            //     string sql = @"SELECT cx.cod_barra
            //               , pi.separado as quantidade
            //               , pi.pdu as pdu
            //               , (pi.preco - pi.valor_mercadoria) as valor_desconto
            //               , pi.valor_mercadoria
            //               , 0 as monitorado
            //                     , CONVERT(VARCHAR, pi.motivo) as motivo
            //, nfpe.RepasseIcms
            //                     , ISNULL(p.origem_produto, 0) AS origem_produto
            //             FROM pedido ped(NOLOCK)
            //                 INNER JOIN pedido_item pi ON ped.cod_pedido = pi.cod_pedido
            //                 INNER JOIN produto p(NOLOCK) ON P.cod_produto = PI.cod_produto
            //                 INNER JOIN caixa_fechada cx(NOLOCK) ON cx.cod_produto = p.cod_produto
            //                 INNER JOIN log_pedido_item_rx_api log(NOLOCK) ON cx.cod_barra = log.cod_produto_rel
            //                                                              AND pi.cod_pedido = log.cod_pedido
            //                 INNER JOIN pedido_oper po(NOLOCK) ON po.cod_pedido = ped.cod_pedido
            //                                                  AND po.cod_operacao = 0
            //                 INNER JOIN entidade ent(NOLOCK) ON ent.cod_entidade = po.cod_entidade
            //                  LEFT JOIN nf_produto_entidade nfpe(NOLOCK) ON nfpe.operacao = ped.operacao
            //                                                            AND nfpe.id_nf_entidade = ent.ID_NF_entidade
            //                                                            AND nfpe.id_nf_produto = p.id_nf_produto
            //                                                            AND nfpe.cod_filial_faturamento = ped.cod_filial_faturamento
            //                      WHERE ped.cod_pedido = @cod_pedido";

            //string sql = @"SELECT lpi.cod_produto_rel
            //                        , ISNULL(pi.separado, 0) AS quantidade
            //                        , ISNULL(pi.pdu, lpi.pdu) AS pdu
            //                        , (ISNULL(pi.preco, 0) - ISNULL(pi.valor_mercadoria, 0)) AS valor_desconto
            //                        , ISNULL(pi.valor_mercadoria, 0) AS valor_mercadoria
            //                        , 0 as monitorado
            //                        , CONVERT(VARCHAR, ISNULL(pi.motivo, 12)) AS motivo
            //                        , ISNULL(nfpe.RepasseIcms, 0) AS RepasseIcms
            //                        , ISNULL(p.origem_produto, 0) AS origem_produto
            //                        FROM log_pedido_rx_api lpr (NOLOCK)
            //                        INNER JOIN log_pedido_item_rx_api lpi (NOLOCK) ON lpr.id_log = lpi.id_log_pedido
            //                        LEFT JOIN pedido ped (NOLOCK) ON ped.cod_pedido = lpr.cod_pedido
            //                        LEFT JOIN pedido_item pi (NOLOCK) ON pi.cod_pedido = lpr.cod_pedido
            //                        LEFT JOIN pedido_oper po (NOLOCK) ON po.cod_pedido = ped.cod_pedido
            //                        AND po.cod_operacao = 0
            //                        LEFT JOIN caixa_fechada cx (NOLOCK) ON cx.cod_barra = lpi.cod_produto_rel
            //                        LEFT JOIN produto p (NOLOCK) ON p.cod_produto = cx.cod_produto
            //                        LEFT JOIN entidade ent (NOLOCK) ON ent.cod_entidade = po.cod_entidade
            //                        LEFT JOIN nf_produto_entidade nfpe (NOLOCK) on nfpe.operacao = ped.operacao
            //                        AND nfpe.id_nf_entidade = ent.ID_NF_entidade
            //                        AND nfpe.id_nf_produto = p.id_nf_produto
            //                        AND nfpe.cod_filial_faturamento = ped.cod_filial_faturamento
            //                        WHERE lpr.id_log = @id_log";

            string sql = @"    SELECT lpi.cod_produto_rel AS cod_barra
                                    , ISNULL(pi.separado, 0) AS quantidade
                                    , ISNULL(pi.pdu, ISNULL(lpi.pdu, 0)) AS pdu
                                    , CASE WHEN (ISNULL(pi.preco, 0) - ISNULL(pi.valor_mercadoria, 0)) < 0 THEN 0
										   ELSE (ISNULL(pi.preco, 0) - ISNULL(pi.valor_mercadoria, 0))
									  END AS valor_desconto
                                    , ISNULL(pi.valor_mercadoria, 0) AS valor_mercadoria
                                    , 0 as monitorado
                                    , CONVERT(VARCHAR, ISNULL(pi.motivo, 12)) AS motivo
                                    , ISNULL(nfpe.RepasseIcms, 0) AS RepasseIcms
                                    , ISNULL(p.origem_produto, 0) AS origem_produto									
                                    FROM log_pedido_rx_api lpr (NOLOCK)
                                    INNER JOIN log_pedido_item_rx_api lpi (NOLOCK) ON lpr.id_log = lpi.id_log_pedido
									LEFT JOIN caixa_fechada cx (NOLOCK) ON cx.cod_barra = lpi.cod_produto_rel
                                    LEFT JOIN pedido ped (NOLOCK) ON ped.cod_pedido = lpr.cod_pedido
                                    LEFT JOIN pedido_item pi (NOLOCK) ON pi.cod_pedido = lpr.cod_pedido
																	 AND pi.cod_produto = cx.cod_produto
                                    LEFT JOIN pedido_oper po (NOLOCK) ON po.cod_pedido = ped.cod_pedido
										  AND po.cod_operacao = 0                                    
                                    LEFT JOIN produto p (NOLOCK) ON p.cod_produto = cx.cod_produto
                                    LEFT JOIN entidade ent (NOLOCK) ON ent.cod_entidade = po.cod_entidade
                                    LEFT JOIN nf_produto_entidade nfpe (NOLOCK) on nfpe.operacao = ped.operacao
										  AND nfpe.id_nf_entidade = ent.ID_NF_entidade
										  AND nfpe.id_nf_produto = p.id_nf_produto
										  AND nfpe.cod_filial_faturamento = ped.cod_filial_faturamento
                                    WHERE lpr.id_log = @id_log";

            var param = new
            {
                id_log
            };

            return await Connection.QueryAsync<OrderProduct>(sql, param);
        }

        public async Task<Order> OrderFiltersQuery(int? cod_pedido, string situacao, string filial)
        {
            try
            {
                string sql = @"SELECT 	ed_cliente.numero as cod_cliente_rel
		                        ,ed_filial.numero as cod_filial_faturamento_rel
                                ,p.cod_filial_estoque
		                        ,cast(p.cod_pedido *10 + dbo.CalcDigitoMod11(p.cod_pedido) as varchar) as cod_pedido
		                        ,p.cod_cond as cod_cond
                                ,p.situacao as situacao
                        FROM	pedido p(NOLOCK)
		                        INNER JOIN pedido_oper po(NOLOCK) ON p.cod_pedido = po.cod_pedido
			                        AND po.cod_operacao = 0
		                        INNER JOIN entidade f ON p.cod_filial_estoque = f.cod_entidade
		                        LEFT JOIN entidade_doc ed_filial(NOLOCK) ON ed_filial.cod_entidade = p.cod_filial_estoque
												                        AND ed_filial.cod_tipo_doc = 3
		                        LEFT JOIN entidade_doc ed_cliente(NOLOCK) ON ed_cliente.cod_entidade = po.cod_entidade
												                        AND ed_cliente.cod_tipo_doc = 3
                                                
                        WHERE   p.situacao in (SELECT valor FROM dbo.testainstr(@situacao)) 
                            AND p.cod_pedido = @cod_pedido";

                var param = new
                {
                    situacao,
                    cod_pedido
                };

                return await Connection.QueryFirstOrDefaultAsync<Order>(sql, param);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Order>> OrderCancellationQuery(string subsidiaryIds, string situacaoCancelamento)
        {
            string sql = @"SELECT	 lpr.cod_industria
							        ,lpr.cod_pedido_secundario
							        ,lpr.cod_filial_faturamento_rel
							        ,id_integracao AS 'Id_Integracao'
                                    ,p.cod_pedido as cod_pedido
                            FROM    log_pedido_rx_api lpr  
							        INNER JOIN pedido p on p.cod_pedido = lpr.cod_pedido
                            WHERE	lpr.data_response IS NOT NULL
		                            AND lpr.cod_pedido IS NOT NULL
		                            AND lpr.data_cancellation IS NULL                            
                                    AND (p.situacao in (SELECT valor FROM dbo.testainstr(@SituacaoCancelamento)))";

            var param = new
            {
                SituacaoCancelamento = situacaoCancelamento
            };

            return await Connection.QueryAsync<Order>(sql, param);
        }

        public async Task<IEnumerable<OrderProduct>> OrderItemsCancellationQuery(int? orderId)
        {
            string sql = @"	    SELECT cx.cod_barra as cod_produto_rel
                                  FROM pedido ped(NOLOCK)
                            INNER JOIN pedido_item pi ON ped.cod_pedido = pi.cod_pedido
                            INNER JOIN produto p(NOLOCK) ON P.cod_produto = PI.cod_produto
	                        INNER JOIN caixa_fechada cx (NOLOCK) ON cx.cod_produto = p.cod_produto
											                        and cx.principal = -1
                        WHERE	ped.cod_pedido = @OrderId";

            var param = new
            {
                OrderId = orderId
            };

            return await Connection.QueryAsync<OrderProduct>(sql, param);
        }

        public async Task<Order> OrderLogIdIntegracaoQuery(string CodPedidoSecundario)
        {
            string sql = @"SELECT id_log,
                                  cod_pedido
                             FROM log_pedido_rx_api
                             WHERE cod_pedido_secundario = @Id";

            var param = new
            {
                @Id = CodPedidoSecundario,
            };

            return await Connection.QueryFirstOrDefaultAsync<Order>(sql, param);
        }
    }
}
