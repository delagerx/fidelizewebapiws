﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Persistence.DapperContext
{
    public abstract class DapperLeitorContext : IDisposable
    {
        protected SqlConnection Connection;

        protected DapperLeitorContext(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();
        }

        public void Dispose()
        {
            if (Connection != null && Connection.State != ConnectionState.Closed)
            {
                Connection.Close();
                Connection = null;
            }

            GC.SuppressFinalize(this);
        }
    }
}
