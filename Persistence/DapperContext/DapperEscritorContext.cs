﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Persistence.DapperContext
{
    public abstract class DapperEscritorContext : IDisposable
    {
        protected SqlConnection Connection;
        protected SqlTransaction Transaction;

        protected DapperEscritorContext(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();
            Transaction = Connection.BeginTransaction();
        }

        protected void Commit()
        {
            try
            {
                Transaction.Commit();
            }
            catch (SqlException ex)
            {
                Transaction.Rollback();
                var message = $"Message: {ex.Message} \n StackTrace: {ex.StackTrace}";
                throw new Exception(message);
            }
            finally
            {
                Transaction.Dispose();
                Transaction = Connection.BeginTransaction();
            }
        }

        protected void Rollback()
        {
            try
            {
                Transaction.Rollback();
            }
            catch (SqlException ex)
            {
                var message = "Message: " + ex.Message + "\n StackTrace: " + ex.StackTrace;
                throw new Exception(message);
            }
            finally
            {
                Transaction.Dispose();
                Transaction = Connection.BeginTransaction();
            }
        }

        public void Dispose()
        {
            if (Transaction != null)
            {
                Transaction.Dispose();
                Transaction = null;
            }

            if (Connection != null && Connection.State != ConnectionState.Closed)
            {
                Connection.Close();
                Connection = null;
            }

            GC.SuppressFinalize(this);
        }
    }
}
