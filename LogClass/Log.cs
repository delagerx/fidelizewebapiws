﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RXWebAPIWS.LogClass
{
    public class Log
    {
        public string Log_Name { get; set; }
        public string Source { get; set; }

        public Log(string logName, string source)
        {
            Log_Name = logName;
            Source = source;
            if (EventLog.SourceExists(Source) == false)
                EventLog.CreateEventSource(Source, Log_Name);
        }

        public void WriteEntry(string input, EventLogEntryType entryType)
        {
            //grava o texto na fonte de logs com o nome que definimos para a constante SOURCE
            EventLog.WriteEntry(Source, input, entryType);
        }

        public void WriteEntry(string input)
        {
            //loga um simples evento com a categoria de informação
            WriteEntry(input, EventLogEntryType.Information);
        }

        public void WriteEntry(Exception ex)
        {
            //loga a ocorrência de uma exceção com a categoria de erro
            WriteEntry(ex.ToString(), EventLogEntryType.Error);
        }
    }
}
