﻿using System;
using System.Collections.Generic;

namespace Core.Models.Order
{
    public class Order : IEquatable<Order>
    {
        public int? cod_pedido { get; set; }
        public string cod_cliente_rel { get; set; }
        public string cod_filial_faturamento_rel { get; set; }
        public int? id_origem { get; set; }
        public decimal? valor_total { get; set; }
        public int? cod_pedido_polo { get; set; }
        public string nome_cliente { get; set; }
        public string cod_filial_estoque_rel { get; set; }
        public int? cod_cobranca { get; set; }
        public decimal? frete { get; set; }
        public decimal? seguro { get; set; }
        public decimal? despesas_acessorias { get; set; }
        public DateTime? data_programado { get; set; }
        public string cod_cond { get; set; }
        public int? id_prazo { get; set; }
        public string obs_nf { get; set; }
        public string obs_pn { get; set; }
        public string obs_Entrega { get; set; }
        public short? operacao { get; set; }
        public int? tipo_importacao_filial { get; set; }
        public int? tipo_importacao_cliente { get; set; }
        public int? tipo_importacao_vendedor { get; set; }
        public int? id_cond { get; set; }
        public int? dias_prazo { get; set; }
        public string cod_pedido_cliente { get; set; }
        public string quem_digitou { get; set; }
        public IEnumerable<OrderProduct> Products { get; set; }        
        public int? cod_pendencia { get; set; }
        public string pendencia { get; set; }
        public string json_input { get; set; }
        public string json_output { get; set; }
        public int? id_log { get; set; }
        public int? id_integracao { get; set; }
        public string cod_pedido_primario { get; set; }
        public string cod_pedido_secundario { get; set; }
        public string cod_industria { get; set; }
        public DateTime? data_cadastro { get; set; }
        public short? situacao { get; set; }
        public int? cod_situacao { get; set; }
        public int? id_prazo_rel { get; set; }

        public bool Equals(Order obj)
        {
            var order = obj as Order;
            return order != null &&
                   EqualityComparer<int?>.Default.Equals(cod_pedido, order.cod_pedido) &&
                   cod_cliente_rel == order.cod_cliente_rel &&
                   cod_filial_faturamento_rel == order.cod_filial_faturamento_rel &&
                   EqualityComparer<int?>.Default.Equals(id_origem, order.id_origem) &&
                   EqualityComparer<decimal?>.Default.Equals(valor_total, order.valor_total) &&
                   EqualityComparer<int?>.Default.Equals(cod_pedido_polo, order.cod_pedido_polo) &&
                   nome_cliente == order.nome_cliente &&
                   cod_filial_estoque_rel == order.cod_filial_estoque_rel &&
                   EqualityComparer<int?>.Default.Equals(cod_cobranca, order.cod_cobranca) &&
                   EqualityComparer<decimal?>.Default.Equals(frete, order.frete) &&
                   EqualityComparer<decimal?>.Default.Equals(seguro, order.seguro) &&
                   EqualityComparer<decimal?>.Default.Equals(despesas_acessorias, order.despesas_acessorias) &&
                   EqualityComparer<DateTime?>.Default.Equals(data_programado, order.data_programado) &&
                   cod_cond == order.cod_cond &&
                   EqualityComparer<int?>.Default.Equals(id_prazo, order.id_prazo) &&
                   obs_nf == order.obs_nf &&
                   obs_pn == order.obs_pn &&
                   obs_Entrega == order.obs_Entrega &&
                   EqualityComparer<short?>.Default.Equals(operacao, order.operacao) &&
                   EqualityComparer<int?>.Default.Equals(tipo_importacao_filial, order.tipo_importacao_filial) &&
                   EqualityComparer<int?>.Default.Equals(tipo_importacao_cliente, order.tipo_importacao_cliente) &&
                   EqualityComparer<int?>.Default.Equals(id_cond, order.id_cond) &&
                   EqualityComparer<int?>.Default.Equals(dias_prazo, order.dias_prazo) &&
                   cod_pedido_cliente == order.cod_pedido_cliente &&
                   quem_digitou == order.quem_digitou &&
                   EqualityComparer<IEnumerable<OrderProduct>>.Default.Equals(Products, order.Products) &&
                   EqualityComparer<int?>.Default.Equals(cod_pendencia, order.cod_pendencia) &&
                   pendencia == order.pendencia &&
                   json_input == order.json_input &&
                   json_output == order.json_output &&
                   EqualityComparer<int?>.Default.Equals(id_log, order.id_log) &&
                   EqualityComparer<int?>.Default.Equals(id_integracao, order.id_integracao) &&
                   cod_pedido_primario == order.cod_pedido_primario &&
                   cod_pedido_secundario == order.cod_pedido_secundario &&
                   cod_industria == order.cod_industria &&
                   EqualityComparer<DateTime?>.Default.Equals(data_cadastro, order.data_cadastro) &&
                   EqualityComparer<short?>.Default.Equals(situacao, order.situacao) &&
                   EqualityComparer<int?>.Default.Equals(cod_situacao, order.cod_situacao) &&
                   EqualityComparer<int?>.Default.Equals(id_prazo_rel, order.id_prazo_rel);
        }

        public override int GetHashCode()
        {
            var hashCode = 1297294706;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_pedido);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_cliente_rel);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_filial_faturamento_rel);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_origem);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(valor_total);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_pedido_polo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nome_cliente);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_filial_estoque_rel);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_cobranca);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(frete);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(seguro);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(despesas_acessorias);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(data_programado);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_cond);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_prazo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obs_nf);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obs_pn);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obs_Entrega);
            hashCode = hashCode * -1521134295 + EqualityComparer<short?>.Default.GetHashCode(operacao);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(tipo_importacao_filial);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(tipo_importacao_cliente);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_cond);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(dias_prazo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_pedido_cliente);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(quem_digitou);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<OrderProduct>>.Default.GetHashCode(Products);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_pendencia);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(pendencia);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(json_input);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(json_output);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_log);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_integracao);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_pedido_primario);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_pedido_secundario);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_industria);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(data_cadastro);
            hashCode = hashCode * -1521134295 + EqualityComparer<short?>.Default.GetHashCode(situacao);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_situacao);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_prazo_rel);
            return hashCode;
        }
    }
}
