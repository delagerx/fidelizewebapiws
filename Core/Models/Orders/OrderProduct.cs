﻿using System;
using System.Collections.Generic;
using Core.Models;

namespace Core.Models.Order
{
    public class OrderProduct : IEquatable<OrderProduct>
    {
        public int? cod_pedido { get; set; }
        public string cod_produto_rel { get; set; }
        public int? quantidade { get; set; }
        public decimal? qb { get; set; }
        public decimal? preco { get; set; }
        public decimal? preco_liq { get; set; }
        public decimal? preco_total { get; set; }
        public decimal? pdu { get; set; }
        public decimal? valor_desconto { get; set; }
        public decimal? pdf { get; set; }
        public string cod_cond { get; set; }
        public int? cod_convenio { get; set; }
        public string cod_filial_estoque_rel { get; set; }
        public int? tipo_importacao_produto { get; set; }
        public string descricao_dif { get; set; }
        public int? quantidade_dif { get; set; }
        public string unidade_dif { get; set; }
        public decimal? preco_dif { get; set; }
        public int? numero_item { get; set; }
        public int? id_cond { get; set; }
        public bool? utilizaDescontoAgrupado { get; set; }
        public bool? utilizaSomaDesconto { get; set; }
        public int? validade_minima { get; set; }
        public int? id_log { get; set; }
        public int? cod_pendencia { get; set; }
        public string pendencia { get; set; }
        public int? id_log_pedido { get; set; }
        public bool? monitorado { get; set; }
        public int? quantidade_atendida { get; set; }
        public decimal? valor_mercadoria { get; set; }
        public decimal? RepasseIcms { get; set; }
        public int? origem_produto { get; set; }
        public string cod_barra { get; set; }
        public string motivo { get; set; }        

        public bool Equals(OrderProduct obj)
        {
            var product = obj as OrderProduct;
            return product != null &&
                   EqualityComparer<int?>.Default.Equals(cod_pedido, product.cod_pedido) &&
                   cod_produto_rel == product.cod_produto_rel &&
                   EqualityComparer<int?>.Default.Equals(quantidade, product.quantidade) &&
                   EqualityComparer<decimal?>.Default.Equals(qb, product.qb) &&
                   EqualityComparer<decimal?>.Default.Equals(preco, product.preco) &&
                   EqualityComparer<decimal?>.Default.Equals(preco_liq, product.preco_liq) &&
                   EqualityComparer<decimal?>.Default.Equals(preco_total, product.preco_total) &&
                   EqualityComparer<decimal?>.Default.Equals(pdu, product.pdu) &&
                   EqualityComparer<decimal?>.Default.Equals(valor_desconto, product.valor_desconto) &&
                   EqualityComparer<decimal?>.Default.Equals(pdf, product.pdf) &&
                   cod_cond == product.cod_cond &&
                   EqualityComparer<int?>.Default.Equals(cod_convenio, product.cod_convenio) &&
                   cod_filial_estoque_rel == product.cod_filial_estoque_rel &&
                   EqualityComparer<int?>.Default.Equals(tipo_importacao_produto, product.tipo_importacao_produto) &&
                   descricao_dif == product.descricao_dif &&
                   EqualityComparer<int?>.Default.Equals(quantidade_dif, product.quantidade_dif) &&
                   unidade_dif == product.unidade_dif &&
                   EqualityComparer<decimal?>.Default.Equals(preco_dif, product.preco_dif) &&
                   EqualityComparer<int?>.Default.Equals(numero_item, product.numero_item) &&
                   EqualityComparer<int?>.Default.Equals(id_cond, product.id_cond) &&
                   EqualityComparer<bool?>.Default.Equals(utilizaDescontoAgrupado, product.utilizaDescontoAgrupado) &&
                   EqualityComparer<bool?>.Default.Equals(utilizaSomaDesconto, product.utilizaSomaDesconto) &&
                   EqualityComparer<int?>.Default.Equals(validade_minima, product.validade_minima) &&
                   EqualityComparer<int?>.Default.Equals(id_log, product.id_log) &&
                   EqualityComparer<int?>.Default.Equals(cod_pendencia, product.cod_pendencia) &&
                   pendencia == product.pendencia &&
                   EqualityComparer<int?>.Default.Equals(id_log_pedido, product.id_log_pedido) &&
                   EqualityComparer<bool?>.Default.Equals(monitorado, product.monitorado) &&
                   EqualityComparer<int?>.Default.Equals(quantidade_atendida, product.quantidade_atendida) &&
                   EqualityComparer<decimal?>.Default.Equals(valor_mercadoria, product.valor_mercadoria) &&
                   EqualityComparer<decimal?>.Default.Equals(RepasseIcms, product.RepasseIcms) &&
                   EqualityComparer<int?>.Default.Equals(origem_produto, product.origem_produto) &&
                   cod_barra == product.cod_barra &&
                   motivo == product.motivo;
        }

        public override int GetHashCode()
        {
            var hashCode = 2099906220;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_pedido);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_produto_rel);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(quantidade);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(qb);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(preco);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(preco_liq);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(preco_total);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(pdu);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(valor_desconto);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(pdf);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_cond);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_convenio);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_filial_estoque_rel);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(tipo_importacao_produto);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(descricao_dif);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(quantidade_dif);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(unidade_dif);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(preco_dif);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(numero_item);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_cond);
            hashCode = hashCode * -1521134295 + EqualityComparer<bool?>.Default.GetHashCode(utilizaDescontoAgrupado);
            hashCode = hashCode * -1521134295 + EqualityComparer<bool?>.Default.GetHashCode(utilizaSomaDesconto);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(validade_minima);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_log);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(cod_pendencia);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(pendencia);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(id_log_pedido);
            hashCode = hashCode * -1521134295 + EqualityComparer<bool?>.Default.GetHashCode(monitorado);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(quantidade_atendida);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(valor_mercadoria);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(RepasseIcms);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(origem_produto);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(cod_barra);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(motivo);
            return hashCode;
        }
    }
}
