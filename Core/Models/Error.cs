﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class Error
    {
        public int ErrorId { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessenger { get; set; }
    }
}
