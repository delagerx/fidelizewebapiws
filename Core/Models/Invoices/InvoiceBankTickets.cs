﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceBankTickets : IEquatable<InvoiceBankTickets>
    {
        public String DocumentNumber { get; set; }
        public DateTime Expires_on { get; set; }
        public DateTime Emission { get; set; }
        public Decimal Value { get; set; }
        public Decimal Installment_value { get; set; }
        public Decimal Anticipation_discount { get; set; }
        public Decimal Interest_daily_rate { get; set; }
        public Decimal Financial_discount { get; set; }
        public Decimal Financial_discount_total_value { get; set; }
        public Decimal Penalty { get; set; }
        public int Bank_code { get; set; }
        public String Bank_agency_code { get; set; }
        public String Bank_account { get; set; }

        public bool Equals(InvoiceBankTickets obj)
        {
            var tickets = obj as InvoiceBankTickets;
            return tickets != null &&
                   DocumentNumber == tickets.DocumentNumber &&
                   Expires_on == tickets.Expires_on &&
                   Emission == tickets.Emission &&
                   Value == tickets.Value &&
                   Installment_value == tickets.Installment_value &&
                   Anticipation_discount == tickets.Anticipation_discount &&
                   Interest_daily_rate == tickets.Interest_daily_rate &&
                   Financial_discount == tickets.Financial_discount &&
                   Financial_discount_total_value == tickets.Financial_discount_total_value &&
                   Penalty == tickets.Penalty &&
                   Bank_code == tickets.Bank_code &&
                   Bank_agency_code == tickets.Bank_agency_code &&
                   Bank_account == tickets.Bank_account;
        }

        public override int GetHashCode()
        {
            var hashCode = -763454188;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(DocumentNumber);
            hashCode = hashCode * -1521134295 + Expires_on.GetHashCode();
            hashCode = hashCode * -1521134295 + Emission.GetHashCode();
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + Installment_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Anticipation_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Interest_daily_rate.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount_total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Penalty.GetHashCode();
            hashCode = hashCode * -1521134295 + Bank_code.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Bank_agency_code);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Bank_account);
            return hashCode;
        }

    }
}
