﻿using Core.Models.Invoices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class Invoice : IEquatable<Invoice>
    {
        public int? InvoiceId { get; set; }
        public int? OrderId { get; set; }
        public int? Wholesaler_Order_Code { get; set; }
        public string Client_CNPJ { get; set; }
        public string Subsidiary_CNPJ { get; set; }
        public int? SubsidiaryId { get; set; }
        public int? Client_Code { get; set; }
        public DateTime? Emission { get; set; }
        public DateTime? ProcessedAt { get; set; }
        public string Invoice_number { get; set; }
        public decimal? Tax_substitution_icms_tax_calc_base { get; set; }
        public decimal? Icms_tax_calc_base { get; set; }
        public int? Invoice_Model { get; set; }
        public int? Invoice_Serie { get; set; }
        public string invoice_danfe_key { get; set; }
        public decimal? Invoice_Value { get; set; }
        public decimal? Invoice_discount { get; set; }
        public decimal? Invoice_passed_along_icms_tax_value { get; set; }
        public decimal? Invoice_products_total_value​ { get; set; }
        public decimal? Invoice_ipi_tax_value { get; set; }
        public decimal? Invoice_freight_value { get; set; }
        public decimal? Invoice_insurance_value { get; set; }
        public decimal? Invoice_icms_tax_total_value { get; set; }
        public decimal? Invoice_icms_tax_retained_total_value { get; set; }
        public decimal? Invoice_volume_amount { get; set; }
        public string Commercial_condition { get; set; }
        public int? Total_products { get; set; }
        public int? StatusOrder { get; set; }        
        public IEnumerable<InvoiceProduct> Products { get; set; }
        public IEnumerable<InvoiceTaxes> Taxes { get; set; }
        public IEnumerable<InvoiceBatches> Batches { get; set; }
        public IEnumerable<InvoiceBankTickets> BankTickets { get; set; }
        public List<Error> Errors { get; set; }
        public string IndustryCode { get; set; }

        protected Invoice()
        {
            Errors = new List<Error>();
        }

        public bool Equals(Invoice obj)
        {
            var invoice = obj as Invoice;
            return invoice != null &&
                   EqualityComparer<int?>.Default.Equals(InvoiceId, invoice.InvoiceId) &&
                   EqualityComparer<int?>.Default.Equals(OrderId, invoice.OrderId) &&
                   Client_CNPJ == invoice.Client_CNPJ &&
                   Subsidiary_CNPJ == invoice.Subsidiary_CNPJ &&
                   EqualityComparer<int?>.Default.Equals(SubsidiaryId, invoice.SubsidiaryId) &&
                   EqualityComparer<int?>.Default.Equals(Client_Code, invoice.Client_Code) &&
                   EqualityComparer<DateTime?>.Default.Equals(Emission, invoice.Emission) &&
                   EqualityComparer<DateTime?>.Default.Equals(ProcessedAt, invoice.ProcessedAt) &&
                   Invoice_number == invoice.Invoice_number &&
                   EqualityComparer<decimal?>.Default.Equals(Tax_substitution_icms_tax_calc_base, invoice.Tax_substitution_icms_tax_calc_base) &&
                   EqualityComparer<decimal?>.Default.Equals(Icms_tax_calc_base, invoice.Icms_tax_calc_base) &&
                   EqualityComparer<int?>.Default.Equals(Invoice_Model, invoice.Invoice_Model) &&
                   EqualityComparer<int?>.Default.Equals(Invoice_Serie, invoice.Invoice_Serie) &&
                   invoice_danfe_key == invoice.invoice_danfe_key &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_Value, invoice.Invoice_Value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_discount, invoice.Invoice_discount) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_passed_along_icms_tax_value, invoice.Invoice_passed_along_icms_tax_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_products_total_value, invoice.Invoice_products_total_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_ipi_tax_value, invoice.Invoice_ipi_tax_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_freight_value, invoice.Invoice_freight_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_insurance_value, invoice.Invoice_insurance_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_icms_tax_total_value, invoice.Invoice_icms_tax_total_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_icms_tax_retained_total_value, invoice.Invoice_icms_tax_retained_total_value) &&
                   EqualityComparer<decimal?>.Default.Equals(Invoice_volume_amount, invoice.Invoice_volume_amount) &&
                   Commercial_condition == invoice.Commercial_condition &&
                   EqualityComparer<int?>.Default.Equals(Total_products, invoice.Total_products) &&
                   EqualityComparer<int?>.Default.Equals(StatusOrder, invoice.StatusOrder) &&
                   EqualityComparer<IEnumerable<InvoiceProduct>>.Default.Equals(Products, invoice.Products) &&
                   EqualityComparer<IEnumerable<InvoiceTaxes>>.Default.Equals(Taxes, invoice.Taxes) &&
                   EqualityComparer<IEnumerable<InvoiceBatches>>.Default.Equals(Batches, invoice.Batches) &&
                   EqualityComparer<IEnumerable<InvoiceBankTickets>>.Default.Equals(BankTickets, invoice.BankTickets) &&
                   EqualityComparer<List<Error>>.Default.Equals(Errors, invoice.Errors) &&
                   IndustryCode == invoice.IndustryCode;
        }

        public override int GetHashCode()
        {
            var hashCode = -1792861437;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(InvoiceId);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(OrderId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Client_CNPJ);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Subsidiary_CNPJ);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(SubsidiaryId);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Client_Code);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(Emission);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(ProcessedAt);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Invoice_number);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Tax_substitution_icms_tax_calc_base);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Icms_tax_calc_base);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Invoice_Model);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Invoice_Serie);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(invoice_danfe_key);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_discount);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_passed_along_icms_tax_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_products_total_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_ipi_tax_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_freight_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_insurance_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_icms_tax_total_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_icms_tax_retained_total_value);
            hashCode = hashCode * -1521134295 + EqualityComparer<decimal?>.Default.GetHashCode(Invoice_volume_amount);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Commercial_condition);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Total_products);
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(StatusOrder);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceProduct>>.Default.GetHashCode(Products);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceTaxes>>.Default.GetHashCode(Taxes);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceBatches>>.Default.GetHashCode(Batches);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<InvoiceBankTickets>>.Default.GetHashCode(BankTickets);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Error>>.Default.GetHashCode(Errors);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(IndustryCode);
            return hashCode;
        }
    }
}
