﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceProduct : IEquatable<InvoiceProduct>
    {
        public int? Id { get; set; }
        public string EAN { get; set; }
        public int? Quantity { get; set; }
        public float? Percent_discount_product { get; set; }
        public float? DiscountValueProduct { get; set; }
        //public string DBC { get; set; }
        public float? UnitPriceProduct { get; set; }
        public int? Id_Commercial_condition { get; set; }
        public decimal? Icms_tax_calc_base { get; set; }
        public decimal? Tax_substitution_icms_tax_calc_base { get; set; }
        public decimal? Icms_percentage { get; set; }
        public decimal? Ipi_percentage { get; set; }
        public decimal? Icms_tax_value { get; set; }
        public decimal? Tax_substitution_value { get; set; }
        public decimal? Passed_along_icms_tax_value { get; set; }
        //public string Payment_term { get; set; }
        public decimal? Passed_along_value { get; set; }
        public decimal? Total_value { get; set; }
        public decimal? Total_value_with_taxes { get; set; }
        public decimal? Financial_discount_percentage { get; set; }
        public decimal? Financial_discount_value { get; set; }
        public int? Tax_substitution_code { get; set; }
        public string Classification { get; set; }
        public int? Cfop_code { get; set; }
        public bool? Monitored { get; set; }

        public bool Equals(InvoiceProduct obj)
        {
            var product = obj as InvoiceProduct;
            return product != null &&
                   EqualityComparer<int?>.Default.Equals(Id, product.Id) &&
                   EAN == product.EAN &&
                   Quantity == product.Quantity &&
                   Percent_discount_product == product.Percent_discount_product &&
                   UnitPriceProduct == product.UnitPriceProduct &&
                   Id_Commercial_condition == product.Id_Commercial_condition &&
                   Icms_tax_calc_base == product.Icms_tax_calc_base &&
                   Tax_substitution_icms_tax_calc_base == product.Tax_substitution_icms_tax_calc_base &&
                   Icms_percentage == product.Icms_percentage &&
                   Ipi_percentage == product.Ipi_percentage &&
                   Icms_tax_value == product.Icms_tax_value &&
                   Tax_substitution_value == product.Tax_substitution_value &&
                   Passed_along_icms_tax_value == product.Passed_along_icms_tax_value &&
                   //Payment_term == product.Payment_term &&
                   Passed_along_value == product.Passed_along_value &&
                   Total_value == product.Total_value &&
                   Total_value_with_taxes == product.Total_value_with_taxes &&
                   Financial_discount_percentage == product.Financial_discount_percentage &&
                   Financial_discount_value == product.Financial_discount_value &&
                   Tax_substitution_code == product.Tax_substitution_code &&
                   Classification == product.Classification &&
                   Cfop_code == product.Cfop_code;
        }

        public override int GetHashCode()
        {
            var hashCode = -1985026123;
            hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + Quantity.GetHashCode();
            hashCode = hashCode * -1521134295 + Percent_discount_product.GetHashCode();
            hashCode = hashCode * -1521134295 + UnitPriceProduct.GetHashCode();
            hashCode = hashCode * -1521134295 + Id_Commercial_condition.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Ipi_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Passed_along_icms_tax_value.GetHashCode();
            //hashCode = hashCode * -1521134295 + Payment_term.GetHashCode();
            hashCode = hashCode * -1521134295 + Passed_along_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Total_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Total_value_with_taxes.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount_percentage.GetHashCode();
            hashCode = hashCode * -1521134295 + Financial_discount_value.GetHashCode();
            hashCode = hashCode * -1521134295 + Tax_substitution_code.GetHashCode();
            hashCode = hashCode * -1521134295 + Classification.GetHashCode();
            hashCode = hashCode * -1521134295 + Cfop_code.GetHashCode();
            return hashCode;
        }

    }
}
