﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceBatches : IEquatable<InvoiceBatches>
    {
        public String EAN { get; set; }
        public int Amount { get; set; }
        public DateTime Expires_on { get; set; }
        public String Batch { get; set; }

        public bool Equals(InvoiceBatches obj)
        {
            var batches = obj as InvoiceBatches;
            return batches != null &&
                   EAN == batches.EAN &&
                   Amount == batches.Amount &&
                   Expires_on == batches.Expires_on &&
                   Batch == batches.Batch;
        }

        public override int GetHashCode()
        {
            var hashCode = 1372612971;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EAN);
            hashCode = hashCode * -1521134295 + Amount.GetHashCode();
            hashCode = hashCode * -1521134295 + Expires_on.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Batch);
            return hashCode;
        }

    }
}
