﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Invoices
{
    public class InvoiceTaxes : IEquatable<InvoiceTaxes>
    {
        public int IdInvoice { get; set; }
        public Decimal Icms_tax_rate { get; set; }
        public Decimal Icms_tax_calc_base { get; set; }
        public Decimal Icms_tax_value { get; set; }

        public bool Equals(InvoiceTaxes obj)
        {
            var taxes = obj as InvoiceTaxes;
            return taxes != null &&
                   IdInvoice == taxes.IdInvoice &&
                   Icms_tax_rate == taxes.Icms_tax_rate &&
                   Icms_tax_calc_base == taxes.Icms_tax_calc_base &&
                   Icms_tax_value == taxes.Icms_tax_value;
        }

        public override int GetHashCode()
        {
            var hashCode = -306120499;
            hashCode = hashCode * -1521134295 + IdInvoice.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_rate.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_calc_base.GetHashCode();
            hashCode = hashCode * -1521134295 + Icms_tax_value.GetHashCode();
            return hashCode;
        }

    }
}
