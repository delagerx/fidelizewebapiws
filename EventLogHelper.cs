using System;
using System.IO;
using System.Configuration;
using System.Diagnostics;

namespace ServiceHelper.LogHelper
{
    public static class EventLogHelper
    {
        public static void LogInfo(string mensagem, bool habilitadoEventLogEventViewer, bool habilitadoEventLogTexto, string source)
        {
            if (habilitadoEventLogEventViewer)
                EventLog.WriteEntry(source, mensagem, EventLogEntryType.Information, 234);

            if (habilitadoEventLogTexto)
            {
                const string directoryLog = "Logs\\Log.txt";
                var caminhoApp = CaminhoAplicacaoHelper.PathAplicacao;

                var file = new FileInfo($"{caminhoApp}\\{directoryLog}");

                file.Directory?.Create();

                var fs = new FileStream(file.FullName, FileMode.Append);
                var sw = new StreamWriter(fs);
                sw.WriteLine(mensagem + "\n");

                sw.Flush();
                sw.Close();
                fs.Close();
            }

            WriteLine(mensagem);
        }

        public static void LogError(string mensagem, bool habilitadoEventLogEventViewer, bool habilitadoEventLogTexto, string source)
        {
            if (habilitadoEventLogEventViewer)
                EventLog.WriteEntry(source, mensagem, EventLogEntryType.Error, 234);

            if (habilitadoEventLogTexto)
            {
                const string directoryLog = "Logs\\LogErro.txt";
                var caminhoApp = CaminhoAplicacaoHelper.PathAplicacao;

                var file = new FileInfo($"{caminhoApp}\\{directoryLog}");

                file.Directory?.Create();

                var fs = new FileStream(file.FullName, FileMode.Append);
                var sw = new StreamWriter(fs);
                sw.WriteLine(mensagem + "\n");

                sw.Flush();
                sw.Close();
                fs.Close();
            }

            WriteFile(mensagem);
        }
        public static void LogInfo(string mensagem, bool habilitadoEventLog, string source)
        {
            if (habilitadoEventLog) EventLog.WriteEntry(source, mensagem, EventLogEntryType.Information, 234);
            WriteLine(mensagem);
        }

        public static void LogError(string mensagem, bool habilitadoEventLog, string source)
        {
            if (habilitadoEventLog) EventLog.WriteEntry(source, mensagem, EventLogEntryType.Error, 234);
            WriteLine(mensagem);
            WriteFile(mensagem);
        }

        private static void WriteFile(string mensagem)
        {
            const string directoryLog = "Logs";
            var caminhoApp = CaminhoAplicacaoHelper.PathAplicacao;

            if (string.IsNullOrEmpty(caminhoApp) || !Directory.Exists($"{caminhoApp}\\{directoryLog}"))
                return;
            if (PermissaoHelper.TemPermissaoEscritaNaPasta($"{caminhoApp}\\{directoryLog}"))
                return;

            var fileName = $"Erro - {DateTime.Now:yyyyMMdd HHmmssfff}.txt";

            using (var file = new StreamWriter($"{caminhoApp}\\{directoryLog}\\{fileName}"))
            {
                file.WriteLine(mensagem);
            }
        }

        private static void WriteLine(string mensagem)
        {
            Console.Out.WriteLine($"{DateTime.Now} - {mensagem}.");
        }
    }
}